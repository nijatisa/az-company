package az.company.exercise.publisher.scheduler;

import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.UUID;

@Component
public class LogAttacker {

    private final KafkaTemplate<String, String> kafkaTemplate;

    public LogAttacker(KafkaTemplate<String, String> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    @PostConstruct
    public void logAttacker() throws InterruptedException {

        while (true) {
            String log = UUID.randomUUID().toString();
            System.out.println(log);
            kafkaTemplate.send("topic-for-log", log);
            Thread.sleep(20000);
        }
    }

}
