# az-company


## DevOps Assignment - Java Application Log Monitoring

## Introduction
I have a Java application that sends random numbers at 20 second intervals. I also have Kafka and ELK. The task is that the application sends the log to Kafka, and Kafka sends to ELK. First of all, make it convenient for you to use and test it, I decided to do with Docker-Compos. In the docker-compos file I install kafka, elk and wrote a Dockerfile for this application. After that, in the logstash/dockerfile and logstash.conf folder, consume messages from the Kafka topic log_topic, and then output them to Elasticsearch.

## Getting Started
These instructions will get the project up and running on your local machine for development and testing purposes.

```
git clone https://gitlab.com/nijatisa/az-company.git
cd az-company
docker-compose up --build -d

```
After up docker compose, you can access Kibana on http://localhost:5601 and setup index pattern for the logs in Kibana.



## Gitlab-CI 
I wrote build and deploy stage. In the build stage i push my image to gitlab container registry. In the deployment stage, I left an example of how to do this, due to the fact that I used the docker compos, I did not write a deploy stage for this. 
