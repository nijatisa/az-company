FROM openjdk:11
COPY . /myapp
WORKDIR /myapp
RUN ./gradlew build
CMD ["java","-jar", "./build/libs/publisher-0.0.1-SNAPSHOT.jar"]